mysql> CREATE DATABASE rezodb DEFAULT CHARACTER SET utf8;

mysql> USE rezodb;

mysql> CREATE TABLE item(item_id int PRIMARY KEY NOT NULL AUTO_INCREMENT,item_name varchar(256)NOT NULL,item_price int NOT NULL,category_id int);

mysql> INSERT INTO item (item_id,item_name,item_price,category_id)VALUES(1,'堅牢な机',3000,1);

mysql> INSERT INTO item (item_id,item_name,item_price,category_id)VALUES(2,'生焼け肉',50,2);

mysql> INSERT INTO item (item_id,item_name,item_price,category_id)VALUES(3,'すっきり分かるJAVA入門',3000,3);

mysql> INSERT INTO item (item_id,item_name,item_price,category_id)VALUES(4,'おしゃれな椅子',2000,1);

mysql> INSERT INTO item (item_id,item_name,item_price,category_id)VALUES(5,'こんがり肉',500,2);

mysql> INSERT INTO item (item_id,item_name,item_price,category_id)VALUES(6,'書き方ドリルSQL',2500,3);

mysql> INSERT INTO item (item_id,item_name,item_price,category_id)VALUES(7,'ふわふわのベッド',30000,1);

mysql> INSERT INTO item (item_id,item_name,item_price,category_id)VALUES(8,'ミラノ風ドリア',300,2);

mysql> SELECT item_name,item_price FROM item WHERE item_price>1000;
+------------------------+------------+
| item_name              | item_price |
+------------------------+------------+
| 堅牢な机               |       3000 |
| すっきり分かるJAVA入門 |       3000 |
| おしゃれな椅子         |       2000 |
| 書き方ドリルSQL        |       2500 |
| ふわふわのベッド       |      30000 |
+------------------------+------------+